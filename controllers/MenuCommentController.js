var MenuComment = require('../models/MenuComment');

module.exports = {
    getAllMenuComments: getAllMenuComments,
    addMenuComment: addMenuComment

}
function getAllMenuComments(req, res) {
    
    var itemId = req.query.itemId;
    console.log(itemId);
    MenuComment
        .find({"menuItemId":itemId})
        .lean()
        .exec(function (err, items) {
            if (err) console.log(err);
                
            else {
                console.dir(items);
                res.json(items)
            }
        });
}


function addMenuComment(req, res) {
    console.log(req.body);
    var commentDetails = {
        userId: req.body.userId,
        comment: req.body.comment,
        picture:req.body.picture,
        menuItemId:req.body.menuItemId
    };

    var commentEntry = new MenuComment(commentDetails)
    commentEntry
        .save(function (err, items) {
            if(err) throw err;
            res.json(items);
        })
};