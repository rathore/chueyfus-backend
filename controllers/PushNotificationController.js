var ionicPushServer = require('ionic-push-server');
var async = require('async');
var PushNotification=require('../models/PushNotification');
var DeviceToken=require('../models/DeviceToken');
var _ = require('underscore');

var customerCredentials = {
    IonicApplicationID: "b23fe439",
    IonicApplicationAPItoken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJiM2M0NmM0ZS0wZTkyLTRjMjQtYTQzZi03NmM5ODJjYTk1ZWMifQ.EZpeAjLFTCs6BeCqMVxLFjzIRKV0DwrcVbJ-sc6b530"
};


module.exports={
    sendNotificationToCustomer:sendNotificationToCustomer,
    getPushNotification:getPushNotification,
    deletePush:deletePush
}

function sendNotificationToCustomer(req,res) {

    var message = req.body.message;
    var messageToSend = req.body.messageToSend;
    // var state = req.body.state;
    var allDeviceToken = [];
    async.series([function(callback){
        DeviceToken.find({}).select('deviceToken').lean().exec(function(err,result){
            if(err){
                res.json(err);
            }
            else{
                allDeviceToken = _.pluck(result,'deviceToken');
                console.dir(allDeviceToken);
                callback();
            }
        })
    },function(callback)
    {
        console.log(message);
        var pushEntry=new PushNotification({message:message});
        pushEntry.save(function(err,result)
        {
            if(err)
            {
                res.send(err);
            }
            else
            {
                res.json(result);
                callback();
            }
        })
    },function(){
        var notification = {
            "tokens": allDeviceToken,
            "profile":"prod",
            "title": "Chuey Fu\'s",
            "notification": {
                "message":messageToSend,
                "payload":{ "$state": 'sidemenu.push'},
                "ios": {
                    "title": "Chuey Fu\'s",
                    "message": messageToSend,
                    "payload":{ "$state": 'sidemenu.push'},
                    "sound": "default",
                    "priority": 10
                },
                "android": {
                    "title": "Chuey Fu\'s",
                    "message": messageToSend,
                    "payload":{ "$state": 'sidemenu.push'},
                    "image": "http://35.160.70.94/img/push_icon.png",
                    "style": "picture",
                    "sound": "default",
                    "icon":"push_icon"
                }
            }
        };
        ionicPushServer(customerCredentials, notification);
    }])
    

}


function getPushNotification(req,res)
{
    PushNotification.find({"deleted":false}).lean().exec(function(err,result)
    {
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.json(result);
        }
    })

}

function deletePush (req, res) {

    var data = req.body;
    var pushId = data.id;

    PushNotification.update({"_id":pushId},{"deleted":true},{upsert:false},function(err,push){
        if(err) throw err;
        else {
            res.json(push);
        }
        // res.send(tour);
    })
}