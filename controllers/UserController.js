var twilioClient = require('twilio')('AC6528a86cd5eaf1c78320a79ff7c17e89', '76661d9963c3a5734e034b6540cf8ea0');
var User = require('../models/User');
var bcrypt = require('bcryptjs')
var _ = require('underscore');
var mailgun = require('mailgun-js')({apiKey: 'key-55926f4a678c42180a02d8e58e7e7e82', domain: 'pixnary.com'});
module.exports = {
    sendOTP: sendOTP,
    userSignUp: userSignUp,
    userLoginMechanism: userLoginMechanism,
    fbSignUp: fbSignUp,
    checkTequila: checkTequila,
    getAllUsers: getAllUsers,
    forgotPassword: forgotPassword,
    changePassword: changePassword,
    // getLoyalty: getLoyalty,
    adminLoginMechanism:adminLoginMechanism,
    setLoyalty: setLoyalty,
    checkUsername:checkUsername
}

function sendOTP(req, res) {
    var phoneNumber = req.body.phoneNumber;
    console.log(phoneNumber);
    var OTP = _.random(1000, 9999);
    console.log(OTP);

    //Send an SMS text message
    //TODO uncomment in production
    //twilioClient.sendMessage({
    //
    //    to: '+91'+phoneNumber, // Any number Twilio can deliver to
    //    from: '+12563443046', // A number you bought from Twilio and can use for outbound communication
    //    body: 'OTP: '+OTP // body of the SMS message
    //
    //}, function(err, responseData) { //this function is executed when a response is received from Twilio
    //
    //    if (!err) { // "err" is an error received during the request, if any
    //
    //        // "responseData" is a JavaScript object containing data received from Twilio.
    //        // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
    //        // http://www.twilio.com/docs/api/rest/sending-sms#example-1
    //
    //        console.log(responseData.from); // outputs "+15097743610" our twilio number
    //        console.log(responseData.body); // outputs "Testing twilio sms" the body of our sms
    //
    //    }
    //});


    res.json({'OTP': OTP});

}

function checkTequila(req, res) {

    var tequilaId = req.body.tequilaId;
    var userId = req.body.userId;
    console.log('tequilaId', tequilaId);
    console.log('userId', userId);

    User
        .update({"_id": userId}, {$push: {"checked": tequilaId}}, {"upsert": true}, function (err, success) {
            if (err) {
                res.json(err);
            }
            else {
                User.find({"_id": userId}).exec(function (error, data) {
                    res.json(data);
                })
            }
        })
}
function setLoyalty(req, res) {

    var loyaltyId = req.body.loyaltyId;
    var loyaltyValue = req.body.loyaltyValue;
    var loyaltyUpdated = req.body.loyaltyUpdated;
    var loyaltyUsed= req.body.loyaltyUsed;

    var userId = req.body.userId;
    console.log('loyaltyId', loyaltyId);
    console.log('loyaltyValue', loyaltyValue);

    console.log('userId', userId);

    User
        .update({"_id": userId,"loyalty.loyaltyId":loyaltyId}, {$set: {"loyalty.$.value": loyaltyValue,"loyalty.$.updated": loyaltyUpdated,"loyalty.$.used": loyaltyUsed}}, {"upsert": false}, function (err, result) {
            if (result.nModified==0) {
                User
                    .update({"_id": userId}, {$push: {"loyalty":{"loyaltyId": loyaltyId,"value":loyaltyValue,"updated": [(new Date()).toString()]}}}, {"upsert": true},function(err,success){
                        if(err){
                            res.json(err);
                        }
                        else{
                            res.json(success);
                        }
                    })
            }
            else{
                res.json(result);
            }
        })
}
function checkUsername(req, res) {

    console.dir(req.query);

    var username = req.query.username.toLowerCase();
    User.findOne({username: username}, function (err, success) {
        if (err) {
            res.json(err);
        }
        else {
            res.json(success);
        }
    })

};

function userSignUp(req, email, password, done) {

    console.dir(req.body.user)

    var user = req.body.user;
    User.findOne({email: user.email}, function (err, document) {
        if (!document) {
            bcrypt.genSalt(10, function (err, salt) {
                if (err) return err;
                // hash the password using our new salt
                bcrypt.hash(user.password, salt, function (err, hash) {
                    if (err)
                        return done(null, false);
                    var userEntry = new User({
                        name: user.name,
                        username: user.username,
                        email: user.email,
                        password: hash,
                        dob: user.dob
                    });
                    userEntry.save(function (err, details) {
                        if (err) {

                            console.log(err)
                        }
                        else {

                            console.log('saved')
                            return done(null, details);
                        }
                    })
                });
            });
        }
        else {

            console.log('user exists');
            return done(null, false);
        }
    })

};
function userLoginMechanism(req, username, password, done) {

    User.findOne({$or:[{username: username},{email: username}]}).exec(
        function (err, document) {
            if (err) {
                console.log(err)
                done(null, false);
            }
            else {
                if (document != null) {
                    bcrypt.compare(password, document.password, function (err, res) {
                        if (res) {
                            console.log("Logged in");
                            return done(null, document);

                        }
                        else {
                            console.log("Invalid Password");
                            return done(null, false);
                        }
                    });
                }
                else {
                    console.log("username does not exists ");
                    //return done(null, false);
                    return done(null, false);
                }

            }

        });

};

function adminLoginMechanism(req, username, password, done) {
    console.log('adminLogin');

    User.findOne({$or:[{username: username},{email: username}],admin:true}).exec(
        function (err, document) {
            if (err) {
                console.log(err)
                done(null, false);
            }
            else {
                if (document != null) {
                    bcrypt.compare(password, document.password, function (err, res) {
                        if (res) {
                            console.log("Logged in");
                            return done(null, document);

                        }
                        else {
                            console.log("Invalid Password");
                            return done(null, false);
                        }
                    });
                }
                else {
                    console.log("username does not exists ");
                    //return done(null, false);
                    return done(null, false);
                }

            }

        });

};
function fbSignUp(req, res) {

    var user = req.body.user;
    console.dir(user);
    User.findOne({fbUserId: user.fbUserId}).lean().exec(function (err, document) {
        if (!document) {

            var userEntry = new User({
                username: user.name,
                name: user.name,
                email: user.email,
                fbUserId: user.fbUserId,
                picture: user.picture
            });
            userEntry.save(function (err, details) {
                if (err) {
                    res.json({'error': err})
                }
                else {
                    console.log('saved');

                    res.json({'userData': details})
                }
            })
        }
        else {

            res.json({'userData': document});
        }
    })

}
// function updateDeviceToken(req, res){
//     var user = req.body;
//     var userId = user._id;
//     var deviceToken = user.deviceToken;
//     console.log('body');
//     console.dir(user);
//     console.log('userId',userId);
//     console.log('deviceToken',deviceToken)
//     User.update({_id:userId},{$set:{deviceToken:deviceToken}},{upsert:false},function(err,result){
//         if(err){
//             res.json(err);
//         }
//         else{
//             res.json(result);
//         }
//     })
// }

function getAllUsers(req, res) {
    User
        .find()
        .exec(function (err, users) {
            res.json(users);
        });
}

function forgotPassword(req, res) {
    console.log(req.body.email);

    var newPassword = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        newPassword += possible.charAt(Math.floor(Math.random() * possible.length));


    var message = {
        from: 'Cheuy Fu\'s <no-reply@chueyfusapp.com>',
        to: req.body.email,
        subject: "Reset password request",
        text: "Cheuy Fu\'s" +
        "\n----------------------------------------" +
        "\n\nYour new password for Cheuy Fu\'s is :" + newPassword +
        "\n\nUse this password for logging in your account." +
        "\n\nAfter logging in, please change your password by clicking on the Settings gear on the top right of menu." +
        "\n\n----------------------------------------" +
        "\nThanks for using Cheuy Fu\'s"
    };

    console.log(newPassword);

    bcrypt.genSalt(10, function (err, salt) {
        if (err) return err;
        // hash the password using our new salt
        bcrypt.hash(newPassword, salt, function (err, hash) {
            User.update({email: req.body.email, fbUserId: {$exists: false}}, {$set: {password: hash}}, {upsert: false},
                function (err, document) {
                    if (document.nModified == 1) {
                        mailgun.messages().send(message, function (error, body) {
                            console.log('sending mail');
                        })
                        res.json({data: 'done'});
                    }
                    else {

                        res.json({data: 'not done'})
                    }

                });
        })
    })


}

function changePassword(req, res) {
    console.log(req.body.email);

    var email = req.body.email;
    var newPassword = req.body.password;
    bcrypt.genSalt(10, function (err, salt) {
        if (err) return err;
        // hash the password using our new salt
        bcrypt.hash(newPassword, salt, function (err, hash) {
            User.update({
                    email: email,
                    fbUserId: {$exists: false}
                }, {$set: {password: hash}}, {upsert: false},
                function (err, document) {
                    if (document.nModified == 1) {
                        res.json({data: 'done'});
                    }
                    else {
                        res.json({data: 'not done'})
                    }
                });
        })
    })

}