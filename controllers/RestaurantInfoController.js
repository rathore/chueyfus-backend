var RestaurantInfo = require('../models/RestaurantInfo');

module.exports = {
    setRestaurantInfo: setRestaurantInfo,
    getRestaurantInfo: getRestaurantInfo,
    editRestaurantInfo:editRestaurantInfo

}
function getRestaurantInfo(req, res) {

    RestaurantInfo.findOne().lean().exec(function(err,result)
    {
        res.json(result);
    })
};

function editRestaurantInfo(req, res) {
    var info=req.body.info;
    console.dir(info);

    RestaurantInfo.update({},{$set:info},{upsert:false},function(err,success){
        console.log(err);
        if(success){
            RestaurantInfo.findOne().lean().exec(function(err,result)
            {
                res.json(result);
            })
        }
        else{
            res.json(err);
        }
    })
    
}


function setRestaurantInfo(req, res) {
    var info=req.body.info;
    console.dir(info);

    var restaurantInfoEntry=new RestaurantInfo(info);
    restaurantInfoEntry.save(function(err,result)
    {
        console.log(err);

        console.log(result);

        res.json(result);
    })

}

var obj=
{
    "name":"Youstart",
    "location":   {"lat":12, "long":12},
    "address":    "jaipur",
    "timing":[
    {"openingTime":'11AM',"closingTime":"10PM"},
    {"openingTime":'11AM',"closingTime":"10PM"},
    {"openingTime":'11AM',"closingTime":"10PM"},
    {"openingTime":'11AM',"closingTime":"10PM"},
    {"openingTime":'11AM',"closingTime":"10PM"},
    {"openingTime":'11AM',"closingTime":"10PM"},
    {"openingTime":'11AM',"closingTime":"10PM"}]
}