var Passcode = require('../models/Passcode');
// var schedule = require('node-schedule');
// schedule.scheduleJob({hour: 08, minute: 0, dayOfWeek: 1}, function () {
//     resetToZeroOnTimer();
// })


module.exports = {
    setPasscode: setPasscode,
    getPasscode: getPasscode,
    updatePasscodeCount: updatePasscodeCount,
    updatePasscodeInfo: updatePasscodeInfo,
    deletePasscode: deletePasscode,
    resetToZero: resetToZero
}
function getPasscode(req, res) {

    Passcode
        .find()
        .exec(function (err, items) {
            res.json(items);
        });
};


function setPasscode(req, res) {

    var passcode = req.body.passcode;
    var name = req.body.name;
    var PasscodeEntry = new Passcode({passcode: passcode, name: name});
    PasscodeEntry.save(function (err, result) {
        if (result)
            res.json({result: result});
        else res.json({error: err})
    })
}


function updatePasscodeCount(req, res) {

    var data = req.body.passcode;
    var today=new Date().getDay();

    var query = {};
    switch (today)
    {
        case 0:
            query = {$inc: {sunday: 1}};
            break;
        case 1:
            query = {$inc: {monday: 1}};
            break;
        case 2:
            query = {$inc: {tuesday: 1}};
            break;
        case 3:
            query = {$inc: {wednesday: 1}};
            break;
        case 4:
            query = {$inc: {thursday: 1}};
            break;
        case 5:
            query = {$inc: {friday: 1}};
            break;
        case 6:
            query = {$inc: {saturday: 1}};
            break;
    }

    Passcode.update({passcode: data}, query, {upsert: false}, function (err, result) {
        res.json(result);
    });
}

function updatePasscodeInfo(req, res) {
    var passcodeInfo = req.body.passcodeInfo;
    console.dir(passcodeInfo);
    Passcode.update({_id: passcodeInfo._id}, {$set:{name:passcodeInfo.name,passcode:passcodeInfo.passcode}}, {upsert: false}, function (err, result) {
        res.json(result);
    });

}

function deletePasscode(req, res) {
    var itemToBeDeletedId = req.params.itemToBeDeletedId;
    Passcode
        .remove({_id: itemToBeDeletedId})
        .exec(function (err, items) {
            res.json(items);
        });
}


function resetToZero(req, res) {
    var itemToBeReset = req.params.itemToBeReset;
    var query = {sunday: 0, monday: 0, tuesday: 0, wednesday: 0, thursday: 0, friday: 0, saturday: 0};

    Passcode.update({_id: itemToBeReset}, {$set: query}, {upsert: false}, function (err, result) {
        console.log(result);
        res.json(result);
    });
}



function resetToZeroOnTimer() {
    var query = {sunday: 0, monday: 0, tuesday: 0, wednesday: 0, thursday: 0, friday: 0, saturday: 0};

    Passcode.update({}, {$set: query}, {upsert: false,multi:true}, function (err, result) {
        console.log(result);
    });
}