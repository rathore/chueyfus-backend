var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;
//Schema Declarations
var CommentSchema = Schema({
        comment: {type: String, required: false, trim: true},
        userId:{type: String, required:true},
        picture:{type: String, required:false},
        deleted: {type: Boolean, default:false}
    },
    {
        timestamps:true,
        collection: "Comment"});

//Deep Population Declarations
CommentSchema.plugin(deepPopulate,{});

//Model Declarations
var Comment = mongoose.model('Comment', CommentSchema);

module.exports=Comment;
