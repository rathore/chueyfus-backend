
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;
//Schema Declarations
var  CategorySchema = Schema({
        name: {type: String, required: false, trim: true},
        image: {type: String, required: true, trim: true},
        imageThumb: {type: String, required: true, trim: true},
        deleted: {type: Boolean, required: true, default: false},
        order: {type: Number, required: false}
    },
    {
            timestamps:true,
            collection: "Category"});

//Deep Population Declarations
 CategorySchema.plugin(deepPopulate,{});

//Model Declarations
var  Category = mongoose.model('Category',  CategorySchema);

module.exports= Category;
