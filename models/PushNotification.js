
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
//Schema Declarations
var PushNotificationSchema = Schema({
        message: {type: String, trim: true},
        deleted: {type: Boolean, required: true, default: false}

    },
    {
            timestamps:true,
            collection: "PushNotification"});


//Model Declarations
var PushNotification = mongoose.model('PushNotification', PushNotificationSchema);

module.exports=PushNotification;
